//
//  AddItemViewController.swift
//  ToDo
//
//  Created by Nelson Chicaiza on 9/5/18.
//  Copyright © 2018 NelsonChicaiza. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {

    @IBOutlet weak var titleTextFiled: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var descriptionTextFiled: UITextField!
    
    var itemManager:ItemManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let itemTitle = titleTextFiled.text ?? ""
        let itemLocation = locationTextField.text ?? ""
        let itemDescription = descriptionTextFiled.text ?? ""
        
        /*let item = Item(
            id:UUID(),
            title: itemTitle,
            location:itemLocation,
            description:itemDescription
        )*/
        
    
        
        
        if itemTitle == ""{
            showAlert(title:"error", message: "title is requerid")
        }
        
        //itemManager.ToDoItems += [item]
        //para guardar en la base de datos
        
        itemManager?.addItem(
            title: itemTitle,
            location: itemLocation,
            itemDescription: itemDescription)
        
    }
    
    func showAlert(title:String, message:String) {//mensaje de alerta cuando no se incerta nada en los campos
        
        let alert = UIAlertController(title: title, message: message,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title:"OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
}

/*if(itemTitle == ""){
   //         print( "El TextField Title está vacío")
       }else{
            print( "Nombre: \(titleTextFiled!)")
        }
        
        if(itemLocation == ""){
            print( "El TextField Location está vacío")
        }else{
            print( "Nombre: \(locationTextField!)")
        }
        
        if(itemDescription == ""){
            print( "El TextField Description está vacío")
        }else{
            print( "Nombre: \(descriptionTextFiled!)")
        }
 */

        

