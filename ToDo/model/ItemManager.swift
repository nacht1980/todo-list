//
//  itemManager.swift
//  ToDo
//
//  Created by Nelson Chicaiza on 9/5/18.
//  Copyright © 2018 NelsonChicaiza. All rights reserved.
//

import Foundation
import RealmSwift

class ItemManager {
    var toDoItem:[Item] = []
    var doneItem:[Item] = []
    var realm:Realm
    
    init() {
        realm = try! Realm()
        print(realm.configuration.fileURL)
    }
    
    func CheckItem(index:Int){
      //  let item = toDoItem.remove(at: index)
      //  doneItem += [item]
        let item = toDoItem[index]
        
        try! realm.write {
            item.done = true
        }
    }
    
    func unCheckItem(index:Int){
      //  let item = toDoItem.remove(at: index)
      //  doneItem += [item]
        
        let item = doneItem[index]
        
        try! realm.write {
            item.done = false
        }
    }
    //para agregar en la base de datos
    func addItem(title:String, location:String, itemDescription:String?){
        let item = Item()
        item.id = "\(UUID())"
        item.title = title
        item.location = location
        item.itemDescription = itemDescription
        
        try! realm.write {
            realm.add(item)
        }
    }
    
    func updateArrays(){
        toDoItem = Array(realm.objects(Item.self).filter("done = false"))
    }
    
    /*func getTodoItems(){
        toDoItem = Array(realm.objects(Item.self).filter("done = false"))
    }
    
    func getDoneItems(){
        doneItem = Array(realm.objects(Item.self).filter("done = true"))
    }*/
}
