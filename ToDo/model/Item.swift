//
//  item.swift
//  ToDo
//
//  Created by Nelson Chicaiza on 9/5/18.
//  Copyright © 2018 NelsonChicaiza. All rights reserved.
//

import Foundation
import RealmSwift

class Item:Object{
    @objc dynamic var id:String?
    @objc dynamic var title:String?
    @objc dynamic var location:String?
    @objc dynamic var itemDescription:String?
    @objc dynamic var done = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

/*struct Item {
    let title: String
    let location: String
    let description: String
}*/
